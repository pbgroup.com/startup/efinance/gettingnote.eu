<?php declare(strict_types=1);

namespace pbgroupeu\gettingnote_eu\tests\Entity\Enterprise;

use pbgroupeu\gettingnote_eu\Entity\Enterprise\Payer;

class PayerTest extends \PHPUnit\Framework\TestCase
{
  /**
   * Unity test
   *
   * @covers Payer
   */
  public function testObjectiveness(): void
  {
    $mock = $this->createMock(Payer::class);

    $this->assertIsObject($mock);
    $this->assertInstanceOf(\JsonSerializable::class, $mock);
  }
}
