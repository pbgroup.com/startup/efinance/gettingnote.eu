<?php declare(strict_types=1);

namespace pbgroupeu\gettingnote_eu\tests\Entity\Enterprise;

use pbgroupeu\gettingnote_eu\Entity\Enterprise\Membership;

class MembershipTest extends \PHPUnit\Framework\TestCase
{
  /**
   * Unity test
   *
   * @covers Payer
   */
  public function testObjectiveness(): void
  {
    $mock = $this->createMock(Membership::class);

    $this->assertIsObject($mock);
    $this->assertInstanceOf(\JsonSerializable::class, $mock);
  }
}
