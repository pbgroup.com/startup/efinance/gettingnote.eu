<?php

namespace pbgroupeu\gettingnote_eu\Entity\Enterprise;

use pbgroupeu\gettingnote_eu\Entity\Friedman\DatingTrait;

/**
 * Payer
 */
class Payer extends \pbgroupeu\gettingnote_eu\Entity\Friedman\BaseEntity implements \JsonSerializable
{
    use DatingTrait;

    /**
     * @var \DateTime
     */
    private $datecreated;

    /**
     * @var \DateTime|null
     */
    private $dateupdated;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \pbgroupeu\gettingnote_eu\Entity\Enterprise\User
     */
    private $user;

    /**
     * @var \pbgroupeu\gettingnote_eu\Entity\Enterprise\Transaction
     */
    private $transaction;

    public function jsonSerialize()
    {
      return [
        'id' => $this->id,
        'datecreated' => $this->datecreated,
        'dateupdated' => $this->dateupdated,
        'user' => $this->user->getEmail(),
        'transaction' => [
          'total' => $this->transaction->getTotal(),
        ],
      ];
    }

    /**
     * Set datecreated.
     *
     * @param \DateTime $datecreated
     *
     * @return Payer
     */
    public function setDatecreated($datecreated)
    {
        $this->datecreated = $datecreated;

        return $this;
    }

    /**
     * Get datecreated.
     *
     * @return \DateTime
     */
    public function getDatecreated()
    {
        return $this->datecreated;
    }

    /**
     * Set dateupdated.
     *
     * @param \DateTime|null $dateupdated
     *
     * @return Payer
     */
    public function setDateupdated($dateupdated = null)
    {
        $this->dateupdated = $dateupdated;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user.
     *
     * @param \pbgroupeu\gettingnote_eu\Entity\Enterprise\User|null $user
     *
     * @return Payer
     */
    public function setUser(\pbgroupeu\gettingnote_eu\Entity\Enterprise\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \pbgroupeu\gettingnote_eu\Entity\Enterprise\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set transaction.
     *
     * @param \pbgroupeu\gettingnote_eu\Entity\Enterprise\Transaction|null $transaction
     *
     * @return Payer
     */
    public function setTransaction(\pbgroupeu\gettingnote_eu\Entity\Enterprise\Transaction $transaction = null)
    {
        $this->transaction = $transaction;

        return $this;
    }

    /**
     * Get transaction.
     *
     * @return \pbgroupeu\gettingnote_eu\Entity\Enterprise\Transaction|null
     */
    public function getTransaction()
    {
        return $this->transaction;
    }
}
